package vueP;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controler.Controleur;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class EcouteurDeBoutons implements ActionListener {
	
    private Controleur controleur;
    final JFileChooser fc = new JFileChooser();

    public EcouteurDeBoutons(Controleur controleur){
        this.controleur = controleur;
    }

    @Override
    public void actionPerformed(ActionEvent e) { 
        // Methode appelee par l'ecouteur de boutons a chaque fois qu'un bouton est clique
        // Envoi au controleur du message correspondant au bouton clique
        String path = null;
        switch (e.getActionCommand()){
            case Fenetre.CHARGER_PLANVILLE:
                FileFilter XMLFilter = new FileNameExtensionFilter("XML File","xml");
                fc.setAcceptAllFileFilterUsed(false);
                fc.addChoosableFileFilter(XMLFilter);
                if(fc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) 
                    path = fc.getSelectedFile().getPath();
                else
                    break;
                controleur.ajouterPlanVille(path); 
                break;
            case Fenetre.CHARGER_LIVRAISON: 
                FileFilter XMLFilter1 = new FileNameExtensionFilter("XML File","xml");
                fc.setAcceptAllFileFilterUsed(false);
                fc.addChoosableFileFilter(XMLFilter1);
                if(fc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) 
                    path = fc.getSelectedFile().getPath();
                else
                    break;
                controleur.ajouterLivraisons(path); 
                break;
            case Fenetre.CALCULER_TOURNEE: controleur.calculTournee(); break;
            case Fenetre.GENERER_FEUILLE_ROUTE: controleur.genererFeuilleRoute(); break;
        }
    }
}
