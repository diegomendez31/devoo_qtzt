package vueP;

import controler.Controleur;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;


import javax.swing.JOptionPane;

public class Fenetre extends JFrame {
	
    // Intitules des boutons de la fenetre
    protected final static String CHARGER_PLANVILLE = "Ajouter un plan de ville";
    protected static final String CHARGER_LIVRAISON = "Ajouter un plan de livraison";
    protected static final String CALCULER_TOURNEE = "Calculer tournée";
    protected static final String GENERER_FEUILLE_ROUTE = "Générer feuille de route";
    private ArrayList<JButton> boutons;
    private JLabel cadreMessages;
    private VueGraphique vueGraphique;
    private VueTextuelle vueTextuelle;
    private EcouteurDeBoutons ecouteurDeBoutons;

    private final String[] intitulesBoutons = new String[]{CHARGER_PLANVILLE, CHARGER_LIVRAISON, 
                    CALCULER_TOURNEE, GENERER_FEUILLE_ROUTE};
    private final int hauteurBouton = 50;
    private final int largeurBouton = 180;
    private final int hauteurCadreMessages = 80;
    private final int largeurVueTextuelle = 400;


    /**
     * Cree une fenetre avec des boutons, une zone graphique pour dessiner le plan p avec l'echelle e, 
     * un cadre pour afficher des messages, une zone textuelle decrivant les formes de p,
     * et des ecouteurs de boutons, de clavier et de souris qui envoient des messages au controleur c
     * @param controleur le controleur
     */
    public Fenetre(int e, Controleur controleur){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setTitle("(H4303) Optimod'Lyon --  Calcul de tournée");
        creeBoutons(controleur);
        cadreMessages = new JLabel();
        cadreMessages.setBorder(BorderFactory.createTitledBorder("Messages..."));
        getContentPane().add(cadreMessages);
        vueGraphique = new VueGraphique(e, this);
        vueTextuelle = new VueTextuelle(this);
       
        setTailleFenetre();
        setVisible(true);
    }

    /**
     * Cree les boutons correspondant aux intitules contenus dans intitulesBoutons
     * cree un ecouteur de boutons qui ecoute ces boutons
     * @param controleur
     */
    private void creeBoutons(Controleur controleur){
        ecouteurDeBoutons = new EcouteurDeBoutons(controleur);
        boutons = new ArrayList<>();
        for (String intit : intitulesBoutons){
            JButton bouton = new JButton(intit);
            boutons.add(bouton);
            bouton.setEnabled(false);
            boutons.get(0).setEnabled(true);
            bouton.setSize(largeurBouton,hauteurBouton);
            bouton.setLocation(0,(boutons.size()-1)*hauteurBouton);
            bouton.setFocusable(false);
            bouton.setFocusPainted(false);
            bouton.addActionListener(ecouteurDeBoutons);
            getContentPane().add(bouton);	
        }
    }

    /**
     * Definit la taille du cadre et de ses composants en fonction de la taille de la vue
     * @param largeurVue
     * @param hauteurVue
     */
    private void setTailleFenetre() {
        int hauteurBoutons = hauteurBouton*intitulesBoutons.length;
        int hauteurFenetre = Math.max(vueGraphique.getHauteur(),hauteurBoutons)+hauteurCadreMessages+40;
        int largeurFenetre = vueGraphique.getLargeur()+largeurBouton+largeurVueTextuelle+30;
        setSize(largeurFenetre, hauteurFenetre);
        cadreMessages.setSize(largeurFenetre-20,hauteurCadreMessages);
        cadreMessages.setLocation(0,hauteurFenetre-hauteurCadreMessages-40);
        vueGraphique.setSize(vueGraphique.getLargeur(), vueGraphique.getHauteur());
        vueGraphique.setLocation(largeurBouton+5, 0);
        vueTextuelle.setSize(largeurVueTextuelle,hauteurFenetre-hauteurCadreMessages-40);
        vueTextuelle.setLocation(10+vueGraphique.getLargeur()+largeurBouton,0);
    }

    /**
     * Affiche message dans la fenetre de dialogue avec l'utilisateur
     * @param message
     */
    public void afficheMessage(String message) {
        cadreMessages.setText(message);
    }

    public void afficheMessageErreur(String message) {
       JOptionPane.showMessageDialog(null,message);
    }
    /**
     * Active les boutons si b = true, les desactive sinon
     * @param b
     */
    public void autoriseBoutons(Boolean b) {
        Iterator<JButton> it = boutons.iterator();
        while (it.hasNext()){
                JButton bouton = it.next();
                bouton.setEnabled(b);
        }
    }

    public int getEchelle(){
        return vueGraphique.getEchelle();
    }

    public void setEchelle(int echelle){
        vueGraphique.setEchelle(echelle);
        setTailleFenetre();
    }

    public VueTextuelle getVueTextuelle() {
        return vueTextuelle;
    }

    public void setVueTextuelle(VueTextuelle vueTextuelle) {
        this.vueTextuelle = vueTextuelle;
    }
    
    
    
    public Observateur getGraphicsObs(){
        return (Observateur) vueGraphique;
    }

    public Observateur getTextObs(){
        return (Observateur) vueTextuelle;
    }
    
    public void updateAffichage(){
        setTailleFenetre();
    }

    public ArrayList<JButton> getBoutons() {
        return boutons;
    }

    public VueGraphique getVueGraphique() {
        return vueGraphique;
    }

    public void setVueGraphique(VueGraphique vueGraphique) {
        this.vueGraphique = vueGraphique;
    }

        
    
}
