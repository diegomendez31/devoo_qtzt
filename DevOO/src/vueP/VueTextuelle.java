package vueP;

import model.VisiteurDeFormes;
import model.Observable;
import model.Itineraire;
import model.PlanVille;
import model.Time;
import model.Troncon;
import model.Tournee;
import model.Livraison;
import model.Intersection;
import javax.swing.BorderFactory;
import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class VueTextuelle extends JPanel implements Observateur, VisiteurDeFormes {

    private String texte;
    private JLabel content;

    /**
     * Cree une vue textuelle de plan dans fenetre
     *
     * @param fenetre
     */
    public VueTextuelle(Fenetre fenetre) {
        super();
        setBorder(BorderFactory.createTitledBorder("------------ Feuille de route : ------------"));
        this.setLayout(new BorderLayout());
        content = new JLabel();
        content.setVerticalTextPosition(JLabel.TOP);
        content.setVerticalAlignment(JLabel.TOP);
        JScrollPane scroller = new JScrollPane();
        scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroller.getViewport().add(content);
        this.add(scroller, BorderLayout.CENTER);
        fenetre.getContentPane().add(this);
        texte = "";
    }

    /**
     * Methode appelee par les objets observes par this a chaque fois qu'ils ont
     * ete modifies
     *
     * @param o
     */
    @Override
    public void update(Observable o) {
        texte = "";
        PlanVille p = (PlanVille) o;
        //System.out.println("UPDATE texte");

        visiteForme(p.getTournee());

        afficheMessage(texte);
    }

    private Time doubleToTime(double x) {
        int h = (int) (x / 3600);
        int m = (int) ((x - h * 3600) / 60);
        int s = (int) ((x - h * 3600) % 60);
        return new Time(h, m, s);
    }

    /**
     * Methode appelee par l'objet visite (un Itineraire) a chaque fois qu'il
     * recoit le message accepte
     *
     * @param i
     */
    @Override
    public void visiteForme(Itineraire i) {
        texte = texte + "<li>";
            //if (c.getEstSelectionne())
        //texte = texte+"<b><i>";
        texte = texte + "Intineraire : Temps de parcours estimé : " + Double.toString(i.getDuree())
                + "/n Départ " + i.getDepart().getId() + " : (" + i.getDepart().getX()
                + "," + i.getDepart().getY() + ")"
                + "/n Départ " + i.getArrive().getId() + " : (" + i.getArrive().getX()
                + "," + i.getArrive().getY() + ")";
            //if (c.getEstSelectionne())
        //texte = texte+"</i></b>";
        texte = texte + "</li>/n";
    }

    /**
     * Methode appelee par l'objet visite (une Livraison) a chaque fois qu'il
     * recoit le message accepte
     *
     * @param l
     */
    @Override
    public void visiteForme(Livraison l) {
        texte = texte + "<li>";
        texte = texte + "<b><i>";
        texte = texte + "Livraison " + l.getIdLiv() + " : adresse=(" + l.getX()
                + "," + l.getY() + ")"
                + " /n <i> Heure de livraison : " + l.getHeureLivraison();
        //if (l.getEstSelectionne())
        texte = texte + "</i></b>";
        texte = texte + "</li>";
        //texte = "<li><br /><i>Du texte  /<i> bien formaté</i><br /></li>" ;
        //System.out.println(texte);
    }

    /**
     * Methode appelee par l'objet visite (une Livraison) a chaque fois qu'il
     * recoit le message accepte
     *
     * @param tournee
     */
    @Override
    public void visiteForme(Tournee tournee) {

        texte = texte + "<html>---------------------- <br/> "
                + "Feuille de route <br/>"
                + "---------------------- <br/> <br/> "
                + "Résumé Itinéraire <br/>"
                + "---------------------- <br/>"
                + "Nombre de livraison : " + tournee.getPlan().getLivraisons().size() + "<br/>"
                + "Départ de l'entrepôt (Point " + tournee.getPlan().getEntrepot().getId() + ") : " + tournee.getStartTime().timeToString() + "<br/> <br/>"
                + "Itinéraires : <br/> <br/>";
        for (int i = 0; i < tournee.getItineraires().size() - 1; i++) {
            Itineraire iti = tournee.getItineraires().get(i);
            Livraison l = tournee.obtenirLivraisonArriveeDepuisItineraire(iti);
            texte = texte + "Itinéraire jusqu'à la livraison : <br/>"
                    + " - Point de Livraison : " + l.getId() + "<br/>"
                    + " - Heure d'arrivée estimée : " + l.getHeureLivraison().timeToString() + "<br/>"
                    + " - Identifiant Client : " + l.getClient() + "<br/>"
                    + "------------------------------------------------ <br/>";
            for (Troncon t : iti.getTroncons()) {
                texte = texte + "  - Depuis le point " + t.getPointA().getId()
                        + " en prenant la rue " + t.getNom() + " jusqu'au point "
                        + t.getPointB().getId() + "<br/>";
            }
            texte = texte + "------------------------------------------------ <br/> <br/>";
        }
        texte = texte + "Itinéraire jusqu'à l'entrepôt : <br/>"
                + " - Point : " + tournee.getPlan().getEntrepot().getId() + "<br/>"
                + " - Heure d'arrivée estimée : " + tournee.getEndTime().timeToString() + "<br/>"
                + "------------------------------------------------ <br/>";

        Itineraire itFinal = tournee.getItineraires().get(tournee.getItineraires().size() - 1);
        for (Troncon t : itFinal.getTroncons()) {
            texte = texte + "  - Depuis le point " + t.getPointA().getId()
                    + " en prenant la rue " + t.getNom() + " jusqu'au point "
                    + t.getPointB().getId() + "<br/>";
        }
        texte = texte + "------------------------------------------------ <br/> <html>";
    }

    @Override
    public void visiteForme(Troncon t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visiteForme(Intersection iter, boolean entrepot) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void afficheMessage(String message) {
        content.setText(content.getText() + message);
    }
}
