package vueP;

import model.Intersection;
import model.VisiteurDeFormes;
import model.Observable;
import model.Itineraire;
import model.PlanVille;
import model.Tournee;
import model.Troncon;
import model.Livraison;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

public class VueGraphique extends JPanel implements Observateur, VisiteurDeFormes {

    private int echelle;
    private int hauteurVue;
    private int largeurVue;
    //private Graphics g;
    private Fenetre f;
    private List<Point> points = new ArrayList<>();
    private List<Point> pointsLiv = new ArrayList<>(); 
    private List<Boolean> timeOkLiv = new ArrayList<>(); 
    private Point entrepot;
    private List<Line2D.Double> lignes = new ArrayList<>();
    private List<Line2D.Double> lignesLiv = new ArrayList<>();
    private List<Integer> coulLiv = new ArrayList<>();
    private int r = 3;

    /**
     * Cree la vue graphique permettant de dessiner plan avec l'echelle e dans
     * la fenetre f
     * @param e l'echelle
     * @param f la fenetre
     */
    public VueGraphique(int e, Fenetre f) {
        super();
        this.echelle = e;  
        hauteurVue = 300;
        largeurVue = 400;
        setLayout(null);
        setBackground(Color.white);
        setSize(largeurVue, hauteurVue);
        this.f = f;
        f.getContentPane().add(this);
        entrepot = null;
    }
        /**
     * Methode appelee par les objets observes par this a chaque fois qu'ils ont
     * ete modifies
     */
    @Override
    public void update(Observable o) {
        PlanVille p = (PlanVille)o;
        int xmax = 0 ;
        int ymax = 0 ;
        int xmin = Integer.MAX_VALUE;
        int ymin = Integer.MAX_VALUE;
        for(Intersection i : p.getIntersections()){
            xmax = Math.max(xmax , i.getX());
            ymax = Math.max(ymax , i.getY());
            xmin = Math.min(xmin , i.getX());
            ymin = Math.min(ymin , i.getY());
        }
        int oldL = largeurVue;
        int oldH = hauteurVue;
        hauteurVue = echelle * (ymax+ymin) / 2 ;
        largeurVue = echelle * (xmax+xmin) / 2 ;
        if(hauteurVue != oldH || largeurVue != oldL)
            f.updateAffichage();
        points.clear();
        pointsLiv.clear();
        lignes.clear();
        lignesLiv.clear();
        coulLiv.clear();
        timeOkLiv.clear();
        entrepot = null;
        
        for(Intersection i : p.getIntersections()){
            visiteForme(i,false);
        }
        
        for(Troncon t : p.getTroncons()){
            visiteForme(t);
        }
        
        for(Livraison l : p.getLivraisons()){
            visiteForme(l);
        }
        if(p.getTournee() != null){
            visiteForme(p.getTournee());
        }
        if(p.getEntrepot()!=null){
            visiteForme(p.getEntrepot(),true);
        }
        repaint();
    }
    /**
     * Methode appelee a chaque fois que VueGraphique doit etre redessinee
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        remplirFond(g);
        dessinerTroncon(g);
        dessinerItineraire(g);
        dessinerIntersection(g);
        dessinerLivraison(g);
        dessinerEntrepot(g);
    }
    /**
     * Rempli le fond de la VueGraphique en gris
     * @param g 
     */
    private void remplirFond(Graphics g){
        g.setColor(Color.lightGray);
        for (int y = 0; y < largeurVue / echelle; y++) {
            g.drawLine(y * echelle, 0, y * echelle, hauteurVue);
        }
        for (int x = 0; x < hauteurVue / echelle; x++) {
            g.drawLine(0, x * echelle, largeurVue, x * echelle);
        }
        g.setColor(Color.gray);
        g.drawRect(0, 0, largeurVue-1, hauteurVue-1);
    }
    /**
     * Dessine tous les troncons en noir
     * @param g 
     */
    private void dessinerTroncon(Graphics g){
        for(Line2D.Double l : lignes){
            g.setColor(Color.black);
            g.drawLine((int)(l.x1/2)*echelle+r, (int)(l.y1/2)*echelle+r, (int)(l.x2/2)*echelle+r, (int)(l.y2/2)*echelle+r);
        }
    }
    
    /**
     * Dessine toutes les itineraires en rouge
     * @param g 
     */
    private void dessinerItineraire(Graphics gr){
        if(coulLiv.size()>0){
            int max = coulLiv.get(coulLiv.size()-1);
            int i=0;

            /*
            *
            *   Gros bout de code emprunté à mon stage
            *   Permet d'avoir une colorMap allant du bleu vers le rouge
            *   (Type Colormap Jet)
            *
            */
            double[] colorMap = new double[3*256];
            colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.515600;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.531300;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.546900;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.562500;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.578100;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.593800;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.609400;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.625000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.640600;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.656300;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.671900;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.687500;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.703100;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.718800;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.734400;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.750000;
            colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.765600;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.781300;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.796900;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.812500;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.828100;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.843800;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.859400;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.875000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.890600;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.906300;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.921900;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.937500;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.953100;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.968800;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.984400;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	1.000000;
            colorMap[i++]=	0.000000;	colorMap[i++]=	0.015600;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.031300;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.046900;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.062500;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.078100;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.093800;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.109400;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.125000;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.140600;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.156300;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.171900;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.187500;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.203100;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.218800;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.234400;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.250000;	colorMap[i++]=	1.000000;
            colorMap[i++]=	0.000000;	colorMap[i++]=	0.265600;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.281300;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.296900;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.312500;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.328100;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.343800;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.359400;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.375000;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.390600;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.406300;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.421900;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.437500;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.453100;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.468800;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.484400;	colorMap[i++]=	1.000000;
            colorMap[i++]=	0.000000;	colorMap[i++]=	0.500000;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.515600;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.531300;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.546900;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.562500;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.578100;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.593800;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.609400;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.625000;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.640600;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.656300;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.671900;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.687500;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.703100;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.718800;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.734400;	colorMap[i++]=	1.000000;
            colorMap[i++]=	0.000000;	colorMap[i++]=	0.750000;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.765600;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.781300;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.796900;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.812500;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.828100;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.843800;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.859400;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.875000;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.890600;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.906300;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.921900;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.937500;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.953100;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.968800;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.000000;	colorMap[i++]=	0.984400;	colorMap[i++]=	1.000000;
            colorMap[i++]=	0.000000;	colorMap[i++]=	1.000000;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.015600;	colorMap[i++]=	1.000000;	colorMap[i++]=	1.000000;		colorMap[i++]=	0.031300;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.984400;		colorMap[i++]=	0.046900;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.968800;		colorMap[i++]=	0.062500;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.953100;		colorMap[i++]=	0.078100;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.937500;		colorMap[i++]=	0.093800;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.921900;		colorMap[i++]=	0.109400;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.906300;		colorMap[i++]=	0.125000;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.890600;		colorMap[i++]=	0.140600;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.875000;		colorMap[i++]=	0.156300;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.859400;		colorMap[i++]=	0.171900;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.843800;		colorMap[i++]=	0.187500;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.828100;		colorMap[i++]=	0.203100;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.812500;		colorMap[i++]=	0.218800;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.796900;		colorMap[i++]=	0.234400;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.781300;
            colorMap[i++]=	0.250000;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.765600;		colorMap[i++]=	0.265600;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.750000;		colorMap[i++]=	0.281300;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.734400;		colorMap[i++]=	0.296900;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.718800;		colorMap[i++]=	0.312500;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.703100;		colorMap[i++]=	0.328100;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.687500;		colorMap[i++]=	0.343800;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.671900;		colorMap[i++]=	0.359400;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.656300;		colorMap[i++]=	0.375000;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.640600;		colorMap[i++]=	0.390600;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.625000;		colorMap[i++]=	0.406300;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.609400;		colorMap[i++]=	0.421900;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.593800;		colorMap[i++]=	0.437500;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.578100;		colorMap[i++]=	0.453100;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.562500;		colorMap[i++]=	0.468800;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.546900;		colorMap[i++]=	0.484400;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.531300;
            colorMap[i++]=	0.500000;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.515600;		colorMap[i++]=	0.515600;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.500000;		colorMap[i++]=	0.531300;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.484400;		colorMap[i++]=	0.546900;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.468800;		colorMap[i++]=	0.562500;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.453100;		colorMap[i++]=	0.578100;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.437500;		colorMap[i++]=	0.593800;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.421900;		colorMap[i++]=	0.609400;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.406300;		colorMap[i++]=	0.625000;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.390600;		colorMap[i++]=	0.640600;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.375000;		colorMap[i++]=	0.656300;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.359400;		colorMap[i++]=	0.671900;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.343800;		colorMap[i++]=	0.687500;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.328100;		colorMap[i++]=	0.703100;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.312500;		colorMap[i++]=	0.718800;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.296900;		colorMap[i++]=	0.734400;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.281300;
            colorMap[i++]=	0.750000;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.265600;		colorMap[i++]=	0.765600;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.250000;		colorMap[i++]=	0.781300;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.234400;		colorMap[i++]=	0.796900;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.218800;		colorMap[i++]=	0.812500;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.203100;		colorMap[i++]=	0.828100;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.187500;		colorMap[i++]=	0.843800;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.171900;		colorMap[i++]=	0.859400;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.156300;		colorMap[i++]=	0.875000;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.140600;		colorMap[i++]=	0.890600;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.125000;		colorMap[i++]=	0.906300;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.109400;		colorMap[i++]=	0.921900;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.093800;		colorMap[i++]=	0.937500;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.078100;		colorMap[i++]=	0.953100;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.062500;		colorMap[i++]=	0.968800;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.046900;		colorMap[i++]=	0.984400;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.031300;
            colorMap[i++]=	1.000000;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.015600;		colorMap[i++]=	1.000000;	colorMap[i++]=	1.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.984400;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.968800;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.953100;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.937500;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.921900;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.906300;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.890600;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.875000;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.859400;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.843800;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.828100;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.812500;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.796900;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.781300;	colorMap[i++]=	0.000000;
            colorMap[i++]=	1.000000;	colorMap[i++]=	0.765600;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.750000;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.734400;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.718800;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.703100;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.687500;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.671900;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.656300;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.640600;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.625000;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.609400;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.593800;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.578100;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.562500;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.546900;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.531300;	colorMap[i++]=	0.000000;
            colorMap[i++]=	1.000000;	colorMap[i++]=	0.515600;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.500000;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.484400;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.468800;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.453100;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.437500;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.421900;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.406300;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.390600;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.375000;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.359400;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.343800;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.328100;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.312500;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.296900;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.281300;	colorMap[i++]=	0.000000;
            colorMap[i++]=	1.000000;	colorMap[i++]=	0.265600;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.250000;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.234400;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.218800;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.203100;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.187500;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.171900;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.156300;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.140600;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.125000;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.109400;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.093800;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.078100;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.062500;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.046900;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.031300;	colorMap[i++]=	0.000000;
            colorMap[i++]=	1.000000;	colorMap[i++]=	0.015600;	colorMap[i++]=	0.000000;		colorMap[i++]=	1.000000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.984400;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.968800;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.953100;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.937500;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.921900;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.906300;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.890600;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.875000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.859400;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.843800;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.828100;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.812500;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.796900;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.781300;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;
            colorMap[i++]=	0.765600;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.750000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.734400;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.718800;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.703100;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.687500;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.671900;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.656300;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.640600;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.625000;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.609400;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.593800;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.578100;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.562500;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.546900;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;		colorMap[i++]=	0.531300;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;
            colorMap[i++]=	0.515600;	colorMap[i++]=	0.000000;	colorMap[i++]=	0.000000;

            int x = 0;
            int count = 0;
            int col = 0;
            for(Line2D.Double l : lignesLiv){

                float red =(float)colorMap[Math.min(x*3,3*256-1)];
                float green =(float)colorMap[Math.min(x*3+1,3*256-1)];
                float blue =(float)colorMap[Math.min(x*3+2,3*256-1)];
                gr.setColor(new Color(red, green, blue));
                count++;
                if(count == coulLiv.get(col)){
                    col++;
                    x += 256/coulLiv.size();
                }
                Graphics2D g2 = (Graphics2D) gr;
                g2.setStroke(new BasicStroke(3));
                g2.draw(new Line2D.Float((int)(l.x1/2)*echelle+r, (int)(l.y1/2)*echelle+r, (int)(l.x2/2)*echelle+r, (int)(l.y2/2)*echelle+r));
                g2.setStroke(new BasicStroke(1));
            }
        }
    }
    /**
     * Dessine toutes les intersections en blanc
     * @param g 
     */
    private void dessinerIntersection(Graphics g){
        for(Point p : points){
            if(!p.equals(entrepot) && !pointsLiv.contains(p)){
                g.setColor(Color.white);
                g.fillOval((p.x/2)*echelle, (p.y/2)*echelle, r*echelle*2, r*echelle*2);
                g.setColor(Color.black);
                g.drawOval((p.x/2)*echelle, (p.y/2)*echelle, r*echelle*2, r*echelle*2);
            }
        }
    }
    /**
     * Dessine l'entrepot en rouge
     * @param g 
     */
    private void dessinerEntrepot(Graphics g){
        if(entrepot != null){
            g.setColor(Color.red);
            g.fillOval((entrepot.x/2)*echelle-2, (entrepot.y/2)*echelle-2, (r*echelle+2)*2, (r*echelle+2)*2);
            g.setColor(Color.black);
            g.drawOval((entrepot.x/2)*echelle-2, (entrepot.y/2)*echelle-2, (r*echelle+2)*2, (r*echelle+2)*2);
        }
    }
    
    /**
     * Dessine toutes les livraisons en bleu
     * @param g 
     */
    private void dessinerLivraison(Graphics g){
        for(int i = 0 ; i < pointsLiv.size() ; i++){
            Point p = pointsLiv.get(i);
            boolean onTime = timeOkLiv.get(i);
            if(onTime)
                g.setColor(Color.MAGENTA);
            else
                g.setColor(Color.PINK);
            g.fillOval((p.x/2)*echelle-1, (p.y/2)*echelle-1, (r*echelle+1)*2, (r*echelle+1)*2);
            g.setColor(Color.black);
            g.drawOval((p.x/2)*echelle-1, (p.y/2)*echelle-1, (r*echelle+1)*2, (r*echelle+1)*2);
        }
    }
    
    public void setEchelle(int e) {
        largeurVue = (largeurVue / echelle) * e;
        hauteurVue = (hauteurVue / echelle) * e;
        setSize(largeurVue, hauteurVue);
        echelle = e;
    }

    /**
     * Methode appelee par l'objet visite (une intersection) a chaque fois qu'il
     * recoit le message update
     * @param intersection
     */
    @Override
    public void visiteForme(Intersection intersection, boolean siEntrepot) {
        if(siEntrepot){
            entrepot = new Point(intersection.getX(),intersection.getY());
        }else{
            points.add(new Point(intersection.getX(),intersection.getY()));
        }
    }

    /**
     * Methode appelee par l'objet visite (un troncon) a chaque fois qu'il
     * recoit le message update
     * @param t
     */
    @Override
    public void visiteForme(Troncon t) {
        lignes.add(new Line2D.Double(t.getPointA().getX(),t.getPointA().getY(),t.getPointB().getX(),t.getPointB().getY()));
    }
    /**
     * Methode appelee par l'objet visite (un itineraire) a chaque fois qu'il
     * recoit le message update
     * @param it 
     */
    @Override
    public void visiteForme(Itineraire it) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * Methode appelee par l'objet visite (une tournee) a chaque fois qu'il
     * recoit le message update
     * @param t 
     */
    @Override
    public void visiteForme(Tournee t) {
        if(!t.getItineraires().isEmpty()){
            int n = 0;
            for (Itineraire i : t.getItineraires()){
                for(Troncon tr : i.getTroncons()){
                    lignesLiv.add(new Line2D.Double(tr.getPointA().getX(),tr.getPointA().getY(),tr.getPointB().getX(),tr.getPointB().getY()));
                    n++;
                }
                coulLiv.add(n);
            }
        }
    }
    
    /**
     * Methode appelee par l'objet visite (une livraison) a chaque fois qu'il
     * recoit le message update
     * @param l 
     */
    @Override
    public void visiteForme(Livraison l) {
        timeOkLiv.add(l.isOnTime());
        Intersection i = (Intersection)l;
        pointsLiv.add(new Point(i.getX(),i.getY()));
    }
    
    public int getEchelle() {
        return echelle;
    }

    public int getHauteur() {
        return hauteurVue;
    }

    public int getLargeur() {
        return largeurVue;
    }
    
}
