/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vueP;

import model.Itineraire;
import model.PlanVille;
import model.Time;
import model.Troncon;
import model.Livraison;
import model.Tournee;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @author Esther
 */
public class FeuilleRoute {
    
    private Tournee tournee;
    
    public FeuilleRoute(PlanVille plan) {
        tournee = plan.getTournee();
    }
    
    public File genererFeuilleRoute() throws FileNotFoundException, UnsupportedEncodingException, IOException {
        
        BufferedWriter output = null;
        File feuilleRoute = new File("Feuille-Route.txt");
        // tournee.CalculTournee();
        try {
            
            output = new BufferedWriter(new FileWriter(feuilleRoute));
            String texte = "";
            texte = texte + "---------------------- \n "
                    + "Feuille de route \n"
                    + "---------------------- \n \n "
                    + "Résumé Itinéraire \n"
                    + "---------------------- \n"
                    + "Nombre de livraison : " + tournee.getPlan().getLivraisons().size() + "\n"
                    + "Départ de l'entrepôt (Point " + tournee.getPlan().getEntrepot().getId() + ") : " + tournee.getStartTime().timeToString() + "\n \n"
                    + "Itinéraires : \n \n";
            for (int i = 0; i < tournee.getItineraires().size() - 1; i++) {
                Itineraire iti = tournee.getItineraires().get(i);
                Livraison l = tournee.obtenirLivraisonArriveeDepuisItineraire(iti);
                texte = texte + "Itinéraire jusqu'à la livraison : \n"
                        + " - Point de Livraison : " + l.getId() + "\n"
                        + " - Heure d'arrivée estimée : " + l.getHeureLivraison().timeToString() + "\n"
                        + " - Identifiant Client : " + l.getClient() + "\n"
                        + "------------------------------------------------ \n";
                for (Troncon t : iti.getTroncons()) {
                    texte = texte + "  - Depuis le point " + t.getPointA().getId()
                            + " en prenant la rue " + t.getNom() + " jusqu'au point "
                            + t.getPointB().getId() + "\n";
                }
                texte = texte + "------------------------------------------------ \n \n";
            }
            texte = texte + "Itinéraire jusqu'à l'entrepôt : \n"
                    + " - Point : " + tournee.getPlan().getEntrepot().getId() + "\n"
                    + " - Heure d'arrivée estimée : " + tournee.getEndTime().timeToString() + "\n"
                    + "------------------------------------------------ \n";
            
            Itineraire itFinal = tournee.getItineraires().get(tournee.getItineraires().size() - 1);
            for (Troncon t : itFinal.getTroncons()) {
                texte = texte + "  - Depuis le point " + t.getPointA().getId()
                        + " en prenant la rue " + t.getNom() + " jusqu'au point "
                        + t.getPointB().getId() + "\n";
            }
            texte = texte + "------------------------------------------------ \n";
            
            output.write(texte);
            
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (output != null) {
                output.close();
            }
        }
        return feuilleRoute;
    }
    
    private Time doubleToTime(double x) {
        int h = (int) (x / 3600);
        int m = (int) ((x - h * 3600) / 60);
        int s = (int) ((x - h * 3600) % 60);
        return new Time(h, m, s);
    }
    
}
