package controler;

import model.PlanVille;
import vueP.Fenetre;


public abstract class EtatDefaut implements Etat{	
    
    /**
     * Ajouter un plan de ville sur le programme à partir d'un ficher.
     * @param fenetre Fenetre
     * @param path Path
     * @param plan PlanVille
     */
    public void ajouterPlanVille(Fenetre fenetre, String path, PlanVille plan){}

    @Override
    public void ajouterLivraisons(Fenetre fenetre, String path, PlanVille plan) throws Exception{}
    
    /**
     * Calcule toute la tournée
     * @param fenetre Fenetre
     * @param plan PlanVille 
     * 
     */
    @Override
    public void calculTournee(Fenetre fenetre, PlanVille plan){}

    @Override
    public void genererFeuilleRoute(Fenetre fenetre, PlanVille plan) throws Exception{}
    
    public void augmenterEchelle(Fenetre fenetre){}
    
    public void  diminuerEchelle(Fenetre fenetre){}
}
