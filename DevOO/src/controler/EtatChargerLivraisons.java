package controler;

import model.PlanVille;
import java.io.File;
import vueP.Fenetre;

/**
 *
 * @author Diego
 */
class EtatChargerLivraisons extends EtatDefaut {

    private final String ERROR_INITIALIATION_LIVRAISON = "Il faut initialiser les livraisons.";

    /**
     * Ajouter les livraisons au plan de ville à partir d'un ficher.
     *
     * @param fenetre Fenetre
     * @param path String
     * @param plan PlanVille
     * @throws java.lang.Exception
     *
     */
    @Override
    public void ajouterLivraisons(Fenetre fenetre, String path, PlanVille plan) throws Exception {
        if (plan != null) {;
            File f = new File(path);

            plan.lireLivraisonsDepuisFichier(f);
            Controleur.setEtatCourant(Controleur.etatInit);
            if (!plan.lireLivraisonsDepuisFichier(f).getNoeud().equals("JourneeType")) {
                fenetre.afficheMessageErreur("Le fichier ne correspond pas à une livraison");
            } else {
                fenetre.afficheMessage("Livraisons ok");
                fenetre.getBoutons().get(2).setEnabled(true);
                fenetre.getBoutons().get(3).setEnabled(false);
            }
        }
    }

    @Override
    public PlanVille ajouterPlanVille(Fenetre fenetre, String path) {
        return null;
    }

}
