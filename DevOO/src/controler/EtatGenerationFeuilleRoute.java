package controler;
import model.PlanVille;
import java.io.File;
import vueP.Fenetre;
import vueP.FeuilleRoute;
/**
 *
 * @author Diego
 */
class EtatGenerationFeuilleRoute extends EtatDefaut{
    
    private final String ERROR_INITIALIATION_FEUILLE = "Il faut initialiser les livraisons.";
    /**
     * Rendre le ficher avec tous les routes.
     * @param fenetre Fenetre
     * @param plan PlanVille
     * @throws java.lang.Exception 
     */
    @Override
    public void genererFeuilleRoute(Fenetre fenetre, PlanVille plan) throws Exception {
        if (plan != null){
           FeuilleRoute feuille = new FeuilleRoute(plan);
           File f = feuille.genererFeuilleRoute();
           Controleur.setEtatCourant(Controleur.etatInit);
           fenetre.afficheMessage("Génération ok");
           // fenetre.autoriseBoutons(true);
        }else{
            Controleur.setEtatCourant(Controleur.etatInit);
           fenetre.afficheMessage("Génération ko");
            fenetre.autoriseBoutons(true);
            throw new Exception(ERROR_INITIALIATION_FEUILLE);
        }
    }

    @Override
    public PlanVille ajouterPlanVille(Fenetre fenetre, String path) {
        return null;
    }
}