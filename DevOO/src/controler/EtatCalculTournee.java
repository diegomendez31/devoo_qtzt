/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;
import model.PlanVille;
import javax.swing.JScrollPane;
import vueP.Fenetre;
import vueP.VueTextuelle;


class EtatCalculTournee extends EtatDefaut{
 
    /**
     * Calcule toute la tournée
     * @param fenetre Fenetre
     * @param plan PlanVille 
     * 
     */
    @Override
    public void calculTournee(Fenetre fenetre, PlanVille plan){
      if (plan != null && plan.getTournee() != null){
            PlanVille p = plan;
            plan.getTournee().CalculTournee();
            plan.notifierObservateurs();
            Controleur.setEtatCourant(Controleur.etatInit);
            fenetre.afficheMessage("Calcul fait");
            VueTextuelle vue = fenetre.getVueTextuelle();
            vue.update(plan);
            
            fenetre.setVueTextuelle(vue);
            plan = p;
            fenetre.getBoutons().get(3).setEnabled(true);
        }else{
            Controleur.setEtatCourant(Controleur.etatInit);
            fenetre.afficheMessage("Calcul pas validé");
            fenetre.autoriseBoutons(true);
        }  
    }

    @Override
    public PlanVille ajouterPlanVille(Fenetre fenetre, String path) {
        return null;
    }
}
