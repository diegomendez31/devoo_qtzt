package controler;


import model.PlanVille;
import vueP.Fenetre;

public interface Etat {
    
    /**
     * Ajouter un plan de ville sur le programme à partir d'un ficher.
     * @param fenetre Fenetre
     * @param path Path
     * @return PlanVille
     */
    public PlanVille ajouterPlanVille(Fenetre fenetre, String path);
    
    /**
     * Ajouter les livraisons au plan de ville à partir d'un ficher. 
     * @param fenetre Fenetre
     * @param path String 
     * @param plan PlanVille
     * @throws java.lang.Exception Exception
     * 
     */
    public void ajouterLivraisons(Fenetre fenetre, String path, PlanVille plan) throws Exception;
    
    /**
     * Calcule toute la tournée
     * @param fenetre Fenetre
     * @param plan PlanVille 
     * 
     */
    public void calculTournee(Fenetre fenetre, PlanVille plan);
    
    /**
     * Rendre le ficher avec tous les routes.
     * @param fenetre Fenetre
     * @param plan PlanVille
     * @throws java.lang.Exception Exception
     */
    public void genererFeuilleRoute(Fenetre fenetre, PlanVille plan) throws Exception;
    /**
     * clic sur le bouton "Augmenter echelle"
     * @param fenetre Fenetre
     */
    public void augmenterEchelle(Fenetre fenetre);
    
    /**
     * clic sur le bouton "Diminuer echelle"
     * @param fenetre Fenetre
     */
    public void diminuerEchelle(Fenetre fenetre);
	
}
