package controler;


import model.PlanVille;

import vueP.Fenetre;

public class EtatInit extends EtatDefaut {
    // Etat initial 

    /*@Override
    public void ajouterPlanVille(Fenetre fenetre, String path, PlanVille plan) {
            fenetre.autoriseBoutons(false);
            fenetre.afficheMessage("Charger Plan");
            Controleur.setEtatCourant(Controleur.etatChargerPlanVille);
    }*/
    
    /**
     * Ajouter les livraisons au plan de ville à partir d'un ficher. 
     * @param fenetre Fenetre
     * @param path String 
     * @param plan PlanVille
     * @throws java.lang.Exception Exception
     * 
     */
    @Override
    public void ajouterLivraisons(Fenetre fenetre, String path, PlanVille plan) throws Exception{
            fenetre.autoriseBoutons(false);
            fenetre.afficheMessage("Charger Livraisons");
            Controleur.setEtatCourant(Controleur.etatChargerLivraisons);
    }
    
    /**
     * Calcule toute la tournée
     * @param fenetre Fenetre
     * @param plan PlanVille 
     * 
     */
    @Override
    public void calculTournee(Fenetre fenetre, PlanVille plan){
            fenetre.autoriseBoutons(false);
            fenetre.afficheMessage("Calcule Tournee");
            Controleur.setEtatCourant(Controleur.etatCalculTournee);
    }
    
    /**
     * Rendre le ficher avec tous les routes.
     * @param fenetre Fenetre
     * @param plan PlanVille
     * @throws java.lang.Exception Exception
     */
    @Override
    public void genererFeuilleRoute(Fenetre fenetre, PlanVille plan) throws Exception{
            fenetre.autoriseBoutons(false);
            fenetre.afficheMessage("Generer Feuille de Route");
            Controleur.setEtatCourant(Controleur.etatGenerationFeuilleRoute);
    }
    
    /**
     * clic sur le bouton "Diminuer echelle"
     * @param fenetre Fenetre
     */
    @Override
    public void diminuerEchelle(Fenetre fenetre) {
            int echelle = fenetre.getEchelle();
            if (echelle > 1){
                    fenetre.setEchelle(fenetre.getEchelle()-1);		}
            else fenetre.afficheMessage("L'echelle ne peut pas etre diminuee.");
    }
    
    /**
     * clic sur le bouton "Augmenter echelle"
     * @param fenetre Fenetre
     */
    @Override
    public void augmenterEchelle(Fenetre fenetre) {
            fenetre.setEchelle(fenetre.getEchelle()+1);
    }
        
      /**
     * Ajouter un plan de ville sur le programme à partir d'un ficher.
     * @param fenetre Fenetre
     * @param path Path
     * @return PlanVille
     */ 
    @Override
    public PlanVille ajouterPlanVille(Fenetre fenetre, String path) {
        return null;
    }
        /*
	@Override
	public void undo(ListeDeCdes listeDeCdes){
		listeDeCdes.undo();
	}

	@Override
	public void redo(ListeDeCdes listeDeCdes){
		listeDeCdes.redo();
	}

	@Override
	public void sauver(Plan plan, Fenetre fenetre){
		try {
			SerialiseurXML.getInstance().sauver(plan);
		} catch (ParserConfigurationException
				| TransformerFactoryConfigurationError
				| TransformerException | ExceptionXML e) {
			fenetre.afficheMessage(e.getMessage());
		}
	}


	@Override
	public void ouvrir(Plan plan, ListeDeCdes listeDeCdes, Fenetre fenetre){
		int largeur = plan.getLargeur();
		int hauteur = plan.getHauteur();
		try {
			DeserialiseurXML.charger(plan);
		} catch (ParserConfigurationException 
				| SAXException | IOException 
				| ExceptionXML | NumberFormatException e) {
			fenetre.afficheMessage(e.getMessage());
			plan.reset(largeur, hauteur);
		}
		listeDeCdes.reset();
	}
*/

}
