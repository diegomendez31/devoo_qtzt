/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import model.PlanVille;
import java.io.File;
import vueP.Fenetre;

/**
 *
 * @author Diego
 */
class EtatChargerPlanVille extends EtatDefaut{

    
    /**
     * Ajouter un plan de ville sur le programme à partir d'un ficher.
     * @param fenetre Fenetre
     * @param path Path
     * 
     */
    @Override
    public PlanVille ajouterPlanVille(Fenetre fenetre, String path){
       File f = new File(path);
        PlanVille plan = new PlanVille(f);
        plan.ajouterObservateur(fenetre.getGraphicsObs());
        //plan.ajouterObservateur(fenetre.getTextObs());
        if (plan.ConstructionPlanVilleAPartirFichier()==4)
        {fenetre.afficheMessageErreur("Le fichier ne correspond pas à un plan ville");}
        else {fenetre.afficheMessage("Plan de ville chargé");
        fenetre.getBoutons().get(1).setEnabled(true);
        fenetre.getBoutons().get(2).setEnabled(false);
        fenetre.getBoutons().get(3).setEnabled(false);
	
        }
        return plan;
    }
}
