package controler;

import model.PlanVille;
import model.Tournee;
import java.util.logging.Level;
import java.util.logging.Logger;
import vueP.*;


/**
 */
public class Controleur {
    /**
     * Message d'erreur d'initialisation.
     */
    private final String ERROR_INITIALIATION_PLAN = "Il faut initialiser le plan de ville.";
    private final String ERROR_INITIALIATION_LIVRAISON = "Il faut initialiser les livraisons.";
    /**
     * Plan de ville sur le programme.
     */
    private PlanVille plan;
    private Fenetre fenetre;
    private static Etat etatCourant;
    protected static final EtatInit etatInit = new EtatInit();
    protected static final EtatChargerPlanVille etatChargerPlanVille = new EtatChargerPlanVille();
    protected static final EtatChargerLivraisons etatChargerLivraisons = new EtatChargerLivraisons();
    protected static final EtatCalculTournee etatCalculTournee = new EtatCalculTournee();
    protected static final EtatGenerationFeuilleRoute etatGenerationFeuilleRoute = new EtatGenerationFeuilleRoute();
    
    private Tournee tourneeCalculee = null;

    /**
     *Constructor
     */
    public Controleur() {
        this.plan = null;
        etatCourant = etatInit;
        /* ctm */
        this.fenetre = null;
    }
    
    /**
     * Ajouter fenetre
     * @param f Fenetre
     */
    public void addFenetre(Fenetre f){
        this.fenetre = f;
    }
    
    /**
     * Change l'etat courant du controleur
     * @param etat le nouvel etat courant
     */
    protected static void setEtatCourant(Etat etat){
	etatCourant = etat;
    }
    
    /**
     * Ajouter un plan de ville sur le programme à partir d'un ficher.
     * @param path String
     * 
     */
    public void ajouterPlanVille(String path) {
        setEtatCourant(etatChargerPlanVille);
        plan = etatCourant.ajouterPlanVille(fenetre, path);
        
    }
    
    /**
     * Ajouter les livraisons au plan de ville à partir d'un ficher. 
     * @param path String 
     */
    public void ajouterLivraisons(String path){
        setEtatCourant(etatChargerLivraisons);
        try {
            etatCourant.ajouterLivraisons(fenetre, path, plan);
        } catch (Exception ex) {
            Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Calcule toute la tournée
     * 
     */
    public void calculTournee() {
        setEtatCourant(etatCalculTournee);
	etatCourant.calculTournee(fenetre, plan);
    }
    
    /**
     * Rendre le ficher avec tous les routes.
     * 
     */
    public void genererFeuilleRoute() {
        setEtatCourant(etatGenerationFeuilleRoute);
        try {
            etatCourant.genererFeuilleRoute(fenetre, plan);
        } catch (Exception ex) {
            Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
	 * Methode appelee par fenetre apres un clic sur le bouton "Diminuer echelle"
	 */
	public void diminuerEchelle(){
		etatCourant.diminuerEchelle(fenetre);
	}

	/**
	 * Methode appelee par fenetre apres un clic sur le bouton "Augmenter echelle"
	 */
	public void augmenterEchelle(){
		etatCourant.augmenterEchelle(fenetre);
	}

    public PlanVille getPlan() {
        return plan;
    }

    public Tournee getTourneeCalculee() {
        return tourneeCalculee;
    }

    public void setPlan(PlanVille plan) {
        this.plan = plan;
    }
    
    
	
	/**
	 * Methode appelee par la fenetre quand l'utilisateur clique sur le bouton "Undo"
	 
	public void undo(){
		etatCourant.undo(listeDeCdes);
	}

	/**
	 * Methode appelee par fenetre apres un clic sur le bouton "Redo"
	
	public void redo(){
		etatCourant.redo(listeDeCdes);
	}
        */
        

    
    
    
    
  

    /**
     * Rendre le plan de ville actuel.
     * @return PlanVille 
     * @throws java.lang.Exception 
     * 
     
    public PlanVille visualiserPlanVille1() throws Exception {
        if (plan != null){
            return plan;
        }else{
            throw new Exception(ERROR_INITIALIATION_PLAN);
        }
    }*/

    

    /**
     * Rendre la liste de livraisons qui sont deja chargées sur le plan de ville.
     * @return ArrayList livraisons
     * @throws java.lang.Exception
     * 
    
    public List<Livraison> visualiserLivraisons1() throws Exception {
        if (plan != null){
            List<Livraison> livraisons;
            /*int nbLivraisons = plan.getNbLivraisons();
            for(int i = 0; i<nbLivraisons; i++){
               livraisons.add(plan.getLivraison(i)); 
            }
            livraisons = plan.getLivraisons();
            return livraisons;
        }else{
            throw new Exception(ERROR_INITIALIATION_LIVRAISON);
        }
    }
 */ 
  

    /**
     * Rendre la tournée deja calculée
     * @param livraison
     * @return 
     * @throws java.lang.Exception 
     
    public Tournee visualiserTournee(Livraison livraison) throws Exception {
        if (plan != null){
           return tourneeCalculee;
        }else{
            throw new Exception(ERROR_INITIALIATION_PLAN);
        }
    }*/


}
