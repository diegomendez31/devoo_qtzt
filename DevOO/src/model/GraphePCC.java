package model;

import java.util.ArrayList;
import java.util.List;
import tsp.Graphe;

/**
 * 
 */
public class GraphePCC implements Graphe {

    private List<Intersection> sommets ;
    private List<Itineraire> couts;
    
    /**
     * Default constructor
     */
    public GraphePCC() {
        sommets = new ArrayList<>();
        couts = new ArrayList<>();
    }


    /**
     *  Ajouter itineraire 
     * 
     * @param unItineraire Itineraire
     */
    public void ajouterItinéraire(Itineraire unItineraire) {
        couts.add(unItineraire);
    }

    /**
     * Rendre Sommets
     * @return integer
     */
    @Override
    public int getNbSommets() {
        int ret = sommets.size();
        return (ret>0)?ret:-1;
    }

    /**
     * Rendre Cout
     * @param a int
     * @param b int
     * @return int 
     */
    @Override
    public int getCout(int a, int b) {
        int ret = -1;
        if(a!=b){
            Itineraire iti = findItineraire(a, b);
            if(iti != null){
                ret = (int) iti.getDuree();
            }
        }
        return ret;
    }

    /**
     * Rendre si est un arc
     * @param a int
     * @param b int
     * @return boolean
     */
    @Override
    public boolean estArc(int a, int b) {
        return findItineraire(a, b) != null;
    }

    /**
     * Ajoute un Sommet
     * @param inter Intersection
     */
    public void ajouterSommet(Intersection inter) {
        if(!sommets.contains(inter))
            sommets.add(inter);
    }
    
    /**
     * Cherche un itineraire
     * @param a int
     * @param b int
     * @return Itineraire
     */
    private Itineraire findItineraire(int a, int b){
        if((a < 0 || a > sommets.size()) && (b < 0 || b > sommets.size()))
            return null;
        
        Intersection deb = sommets.get(a);
        Intersection fin = sommets.get(b);
        
        Itineraire ret = null;
        
        for(Itineraire i : couts){
            if(i.relie(deb, fin))
                ret = i;
        }
        
        return ret;
    }
    
    /**
     * Rendre une intersection
     * @param rank int
     * @return Intersection
     */
    public Intersection getIntersectionByRank(int rank){
        return sommets.get(rank);
    }
}