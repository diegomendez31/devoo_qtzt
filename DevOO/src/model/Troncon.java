package model;

/**
 * 
 */
public class Troncon {

    /**
     * Constructor
     * @param nom String
     * @param vitesse double
     * @param longeur double
     * @param pointA Intersection
     * @param pointB Intersection
     */
    
      public Troncon(String nom, double vitesse, double longeur, Intersection pointA, Intersection pointB) {
        this.nom=nom;
        this.vitesse=vitesse;
        this.longeur=longeur;
        this.pointA = pointA;
        this.pointB = pointB;
    }

    /**
     * nom du troncon
     */
    private String nom;

    /**
     * vitesse du troncon
     */
    private double vitesse;

    /**
     * longueur du toncon
     */
    private double longeur;
    
    /**
     * point de depart
     */
    private Intersection pointA;

    /**
     * point d'arrivee
     */
    private Intersection pointB;
    
    /**
     * Rendre point A
     * @return Intersection
     */
    public Intersection getPointA() {
        return pointA;
    }
    
    /**
     * Rendre point B
     * @return Intersection
     */
    public Intersection getPointB() {
        return pointB;
    }
   
    
    /**
     * Calcule de duree
     * calcul de la duree avec le ratio: longueur/vitesse
     * @return la duree du troncon
     */
    public double calculDuree(){
        return longeur/vitesse;
    }

    @Override
    public String toString() {
        return "Troncon{" + "nom=" + nom + ", vitesse=" + vitesse + ", longeur=" + longeur + ", pointA=" + pointA + ", pointB=" + pointB + '}';
    }
    
    /**
     * Rendre le nom
     * @return String
     */
    public String getNom() {
        return nom;
    }
    
    
    
}