package model;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 */
public class Intersection {

    /**
     * Default constructor
     */
  
    /**
     * identité du point
     */
    private int id;

    /**
     * abscisse
     */
    private int x;

    /**
     * ordonné
     */
    private int y;
    
    /**
     * la liste des troncons existants concentant ce point
     * 
     */
    private List<Troncon> troncons;


    /**
     * Constructor
     * @param idi int
     */
    
    public Intersection(int idi){
        id=idi;
        x=-1; // on initialise x et y à -1 lorsqu'on connait pas encore leurs coordonnées
        y=-1;
        troncons = new ArrayList<>();
    }
    
    /**
     * Constructor
     * @param xi int
     * @param yi int
     * @param idi int
     */
    public Intersection(int xi, int yi, int idi) {
        // TODO implement here
        id=idi;
        x=xi;
        y=yi;
        troncons = new ArrayList<>();
    }
    
    /**
     * Constructor
     * @param orig Intersection
     */
    public Intersection(Intersection orig){
        id  = orig.id;
        x   = orig.x;
        y   = orig.y;
        troncons = orig.troncons;
    }
    
    /**
     * Ajoute un troncon a l'intersection
     * @param t Troncon
     */
    public void AjouterTroncon(Troncon t) {
        // TODO implement here
        troncons.add(t);
    }
    
    /**
     * Rendre un troncon voisin
     * @param voisin Intersection
     * @return Troncon
     */
    public Troncon getTronconToVoisin(Intersection voisin){
        for(Troncon t : troncons){
            if(t.getPointB().equals(voisin) || t.getPointA().equals(voisin))
                return t;
        }
        return null;
    }

    /**
     * Rendre voisins
     * @return un vecteur qui contient les points de type Intersection 
     * 
     */
    public List<Intersection> getvoisinsAccessible() {
        // TODO implement here
        List<Intersection> voisins= new ArrayList<>();
        for (Troncon t : troncons){
            Intersection a = t.getPointA();
            Intersection b = t.getPointB();
            if(a.equals(this))
                voisins.add(b);
        }
        return voisins;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        final Intersection other = (Intersection) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        return true;
    }
    /**
     * rendre id
     * @return id
     */
    public int getId() {
        return id;
    }
    /**
     * rendre x
     * @return x
     */
    public int getX() {
        return x;
    }
    /**
     * rendre y
     * @return y
     */
    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Intersection{" + "id=" + id + ", x=" + x + ", y=" + y + '}';
    }
    

    
}