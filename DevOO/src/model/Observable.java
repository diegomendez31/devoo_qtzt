package model;

import vueP.Observateur;

/**
 * 
 */
public interface Observable {

    /**
     * Notifier les observateurs
     * 
     */
    public void notifierObservateurs();

    /**
     * Supprimer l'observateur suivant
     * @param unObservateur Observateur
     */
    public void supprimerObservateur(Observateur unObservateur);

    /**
     * Ajoute l'observateur
     * @param obs OBervateur
     */
     public void ajouterObservateur(Observateur obs);

}