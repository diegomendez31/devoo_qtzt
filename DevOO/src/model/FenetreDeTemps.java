package model;


import java.util.ArrayList;

/**
 * 
 */
public class FenetreDeTemps {
    

    private final String ERROR_HEURE = "Heure du fin non valide";
    
    /**
     * Heure de debut de la fenetre de livraisons
     */
    private Time heureDebut = null;

    /**
     * Heure de fin de la fenetre de livraisons
     */
    private Time heureFin = null;

    /**
     * Liste de livraisons dans la meme fenetre.
     */
    private ArrayList<Livraison> livraisons;




    /**
     * Constructor
     * @param debut Time
     * @param fin Time
     * @throws java.lang.Exception Exception
     */
    public FenetreDeTemps(Time debut, Time fin) throws Exception {
        heureDebut = debut;
        heureFin = fin;
        if(fin.getHours()> debut.getHours()){
            /* ctm */
        }else if(fin.getHours() == debut.getHours()){
            if(fin.getMinutes()>debut.getMinutes()){
                /* ctm */
            }else if(fin.getMinutes() == debut.getMinutes()){
                if(fin.getSeconds()>debut.getSeconds()){
                    /* ctm */
                } else throw new Exception(ERROR_HEURE);
            } else throw new Exception(ERROR_HEURE);
        }else throw new Exception(ERROR_HEURE);
        livraisons = new ArrayList<> ();
    }
    

    /**
     * Ajouter une livraison à la fenetre.
     * @param liv Livraison
     */
    public void ajouterLivraison(Livraison liv) {
         this.livraisons.add(liv);
    }
    
    

    /**
     * Rendre la liste de livraisons dans la fenetre
     * @return ArrayList livraisons
     */
    public ArrayList<Livraison> getLivraisons() {
        return livraisons;
    }
    
    /**
     * Rendre l'heure de debut
     * @return Time
     */
    public Time getHeureDebut() {
        return heureDebut;
    }
    
    /**
     * Rendre l'heure de debut
     * @return Time
     */
    public Time getHeureFin() {
        return heureFin;
    }

    @Override
    public String toString() {
        return "FenetreDeTemps : " + heureDebut + " -" + heureFin;
    }

    
}
