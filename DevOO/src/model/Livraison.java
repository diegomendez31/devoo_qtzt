package model;


/**
 * 
 */
public class Livraison extends Intersection {

    /**
     * L'id de la livraison
     */
    private int idLiv;

    /**
     * Le client servi lors de cette livraison
     */
    private int client;

    /**
     * heure de passage de la livraison
     */
    private Time heurePassage;

     /**
     * la fenetre liée à cette livraison
     */
    private FenetreDeTemps fen;


    /**
     * Ces params prénsentent l'initialisation des attributs à travers le
     * constructeur
     * @param unId int
     * @param unClient int
     * @param adresse Intersection
     * @param f FenetreDeTemps
     */
    public Livraison(int unId, int unClient, Intersection adresse, FenetreDeTemps f) {
        super(adresse);
        idLiv=unId;
        client=unClient;
        fen=f;
    }

    /**
     * Mettre l'heure de livraison
     * @param h Time
     * @return Time
     */
    public Time setHeureLivraison(Time h) {
        // TODO implement here
        if(h.before(fen.getHeureDebut()))
            heurePassage = new Time(fen.getHeureDebut()); 
        else
            heurePassage = new Time(h);
        
        return heurePassage;
    }
    
    /**
     * rendre l'id de livraison
     * @return int
     */
    public int getIdLiv(){
        return idLiv;
    }
    
    /**
     * rendre l'heure de livraison
     * @return time
     */
    public Time getHeureLivraison(){
        return heurePassage;
    }
    
    /**
     * rendre le client de livraison
     * @return int
     */
    public int getClient() {
        return client;
    }
    
    /**
     * rendre l'heure de passage
     * @return Time
     */
    public Time getHeurePassage() {
        return heurePassage;
    }
    
    /**
     * rendre la fenetre de livraison
     * @return FenetreDeTemp
     */
    public FenetreDeTemps getFen() {
        return fen;
    }
    
    /**
     * Il va dire si la livraison est a l'heure indique
     * @return boolean
     */
    public boolean isOnTime(){
        if(heurePassage == null)
            return true;
        return heurePassage.before(getFen().getHeureFin());
    }
}
