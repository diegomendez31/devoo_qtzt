package model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import tsp.TSP;
import tsp.TSP1;

/**
 * 
 */
public class Tournee {

    
    private List<FenetreDeTemps> fenetresDeTemps;
    private PlanVille plan;
    private List<Itineraire> itineraires;
    private Time startTime;
    private Time endTime;
    private String noeud;
    
    /**
     * Calcule la duree de la tournee
     * @return int
     */
    private int CalculDuree() {
        if(startTime != null){
            return 2;
        }
        GraphePCC graphe = new GraphePCC();
        graphe.ajouterSommet(plan.getEntrepot());
        
        for(FenetreDeTemps fenetre : fenetresDeTemps){
            for(Livraison l : fenetre.getLivraisons()){
                graphe.ajouterSommet(l);
            }
        }
        
        List<Itineraire> itineraireAll = new ArrayList<>();
        
        List<Intersection> intersections = new ArrayList<>();
        
        for(Livraison l : fenetresDeTemps.get(0).getLivraisons()){
            Intersection i = plan.getIntersectionByiD(l.getId());
            intersections.add(i);
        }
        
        itineraireAll.addAll(plan.CalculPlusCourtChemin(plan.getEntrepot(),intersections));

        for(int i = 0 ; i < fenetresDeTemps.size() ; i++){
            FenetreDeTemps f1 = fenetresDeTemps.get(i);
            
            for(Livraison start : f1.getLivraisons()){
                List<Intersection> interLoc = new ArrayList<>();
                for(Livraison l : f1.getLivraisons()){
                    if(start != l)
                        interLoc.add((Intersection)l);
                }
                
                if(i < fenetresDeTemps.size()-1){
                    FenetreDeTemps f2 = fenetresDeTemps.get(i+1);
                    for(Livraison l : f2.getLivraisons())
                        interLoc.add((Intersection)l);
                }
                else{
                    interLoc.add(plan.getEntrepot());
                }
                itineraireAll.addAll(plan.CalculPlusCourtChemin(start, interLoc));
                for(Intersection inter : interLoc){
                    if(!intersections.contains(inter))
                        intersections.add(inter);
                }
            }
        }
        
        for(Itineraire it : itineraireAll){
            graphe.ajouterItinéraire(it);
        }
        
        TSP tsp = new TSP1();
        
        tsp.chercheSolution(60000, graphe);
        
        List<Intersection> order = new ArrayList<>();
        
        order.add(plan.getEntrepot());
        for(int j = 1 ; j < graphe.getNbSommets() ; j++){
            order.add(graphe.getIntersectionByRank(tsp.getSolution(j)));
        }
        order.add(plan.getEntrepot());
        
        for(int i = 0 ; i < order.size()-1 ; i++){
            Intersection a = order.get(i);
            Intersection b = order.get(i+1);
            
            Itineraire it = findItineraireInList(a, b, itineraireAll);
            
            itineraires.add(it);
        }
       
        
        
        // TODO calcul des temps
        
        startTime = new Time(fenetresDeTemps.get(0).getHeureDebut());
        
        startTime.remTime(new Time(itineraires.get(0).getDuree()));
        
        Time curTime = new Time(fenetresDeTemps.get(0).getHeureDebut());
        
        curTime = obtenirLivraisonArriveeDepuisItineraire(itineraires.get(0)).setHeureLivraison(new Time(curTime));
        
        System.out.println(obtenirLivraisonArriveeDepuisItineraire(itineraires.get(0)));
        for(int i = 1 ; i < itineraires.size() - 1 ; i++){
            Itineraire iti = itineraires.get(i);
            Livraison next = obtenirLivraisonArriveeDepuisItineraire(iti);
            double dur = iti.getDuree();
            Time nextTime = new Time(curTime);
            nextTime.addTime(0,10,0);
            curTime = next.setHeureLivraison(nextTime.addTime(new Time(dur)));
        }
        curTime.addTime(0,10,0);
        double lastDur = itineraires.get(itineraires.size()-1).getDuree();
        Time nextTime = new Time(curTime);
        endTime = nextTime.addTime(new Time(lastDur));
        return 0;
    }
    
    /**
     * Mettre la liste d'itineraires sur la tournee
     * @param itineraires List
     */
    public void setItineraires(List<Itineraire> itineraires) {
        this.itineraires = itineraires;
    }
    
    public Livraison obtenirLivraisonArriveeDepuisItineraire(Itineraire i){
        for(FenetreDeTemps fdt : fenetresDeTemps){
            for(Livraison l : fdt.getLivraisons()){
                if(i.getArrive().equals(l)){
                    return l;
                }
            }
        }
        return null;
    }
    
    /**
     * Cherche la livraison sur l'intersection
     * @param inter Intersection
     * @return Livraison
     */
    private Livraison findLivraisonFromInter(Intersection inter){
        for(FenetreDeTemps fen : fenetresDeTemps){
            int rank = fen.getLivraisons().lastIndexOf(inter);
            if(rank != -1)
                return fen.getLivraisons().get(rank);
        }
        return null;
    }
    
    /**
     * Cherche l'itineraire sur la liste
     * @param start Intersection
     * @param end Intersection
     * @param list List
     * @return Itineraire
     */
    private Itineraire findItineraireInList(Intersection start , Intersection end , List<Itineraire> list){
        for(Itineraire i : list){
            if(i.relie(start, end))
                return i;
        }
        return null;
    }
    
    /**
     * Calcule tournee sur le plan
     */
    public void CalculTournee() {
        // TODO implement here
        CalculDuree();
    }
    
    /**
     * Constructor
     *
     */
    
    public Tournee(){
        plan = null;
        startTime = null;
        endTime = null;
        fenetresDeTemps = null;
        itineraires = null;
    }
    
    /**
     * Constructor
     * @param livraisons File
     * @param p PlanVille
     */
    public Tournee(File livraisons , PlanVille p) {
        plan = p;
        startTime = null;
        endTime = null;
        itineraires = new ArrayList<>();
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(livraisons);
            Element racine=document.getDocumentElement();
            noeud=racine.getNodeName();
            if(!noeud.equals("JourneeType"))
                return;
            NodeList l = document.getElementsByTagName("Plage");

            fenetresDeTemps = new ArrayList<>();            
            
            for (int j=0; j<l.getLength(); ++j) {
                Node prop = l.item(j);
                NamedNodeMap attr = prop.getAttributes();
                if (null != attr) {
                    Node nHD = attr.getNamedItem("heureDebut");
                    String sHD = nHD.getNodeValue();
                    Node nHF = attr.getNamedItem("heureFin");
                    String sHF = nHF.getNodeValue();
                    int hH ,hM ,hS;
                    String[] split = sHD.split(":");
                    hH = new Integer(split[0]);
                    hM = new Integer(split[1]);
                    hS = new Integer(split[2]);
                    Time heureDebut = new Time(hH,hM,hS);
                    String[] split2 = sHF.split(":");
                    hH = new Integer(split2[0]);
                    hM = new Integer(split2[1]);
                    hS = new Integer(split2[2]);
                    Time heureFin = new Time(hH,hM,hS);
                    
                    fenetresDeTemps.add(new FenetreDeTemps(heureDebut,heureFin));
                }
            }
            
            for (int j=0; j<l.getLength(); ++j) {
                Node prop = l.item(j);
                // ---- Pourquoi item(1) : car présence de retours à la ligne dans le fichier xml
                Node nodeLivs = prop.getChildNodes().item(1);
                NodeList listLivs = nodeLivs.getChildNodes();
                FenetreDeTemps f = fenetresDeTemps.get(j);
                
                for(int i = 0 ; i <listLivs.getLength() ; ++i){
                    Node nodeLiv = listLivs.item(i);
                    NamedNodeMap attr = nodeLiv.getAttributes();
                    if (null != attr) {
                        String id = attr.getNamedItem("id").getNodeValue();
                        String client = attr.getNamedItem("client").getNodeValue();
                        String adresse = attr.getNamedItem("adresse").getNodeValue();
                        
                        plan.CreerLivraison(new Integer(id), new Integer(client), new Integer(adresse), f);
                        
                    }
                    
                    
                }
                
            }
            
            Node e = document.getElementsByTagName("Entrepot").item(0);
            NamedNodeMap attrE = e.getAttributes();
            
            plan.setEntrepot(new Integer(attrE.getNamedItem("adresse").getNodeValue()));
            
            
        } catch (SAXException ex) {
            Logger.getLogger(Tournee.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Tournee.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Tournee.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Tournee.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Mettre les fenetre de temps
     * @param fenetresDeTemps List
     * 
     */
    public void setFenetresDeTemps(List<FenetreDeTemps> fenetresDeTemps) {
        this.fenetresDeTemps = fenetresDeTemps;
    }
    /**
     * Rendre la liste itineraires
     * @return liste itineraires
     */
    
    
    
    public List<Itineraire> getItineraires() {
        return itineraires;
    }
    
    /**
     * Rendre le plan ville
     * @return liste itineraires
     */
    public PlanVille getPlan() {
        return plan;
    }
    
    /**
     * Rendre fenetres de temps
     * @return List 
     */
    public List<FenetreDeTemps> getFenetresDeTemps() {
        return fenetresDeTemps;
    }
    
    /**
     * Rendre temps de debut
     * @return Time 
     */
    public Time getStartTime() {
        return startTime;
    }
    
    /**
     * Rendre temps du fin
     * @return Time 
     */
    public Time getEndTime() {
        return endTime;
    }
    
    /**
     * Rendre noeud 
     * @return String 
     */
    public String getNoeud() {
        return noeud;
    }

    
    
    
    

}