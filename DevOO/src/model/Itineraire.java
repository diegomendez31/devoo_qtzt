package model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
public class Itineraire {

    private double duree ;
    private Intersection depart;
    private Intersection arrive;
    private List<Troncon> troncons;
    
    /**
     * Constructor
     * @param depart Intersection
     * @param arrive Intersection
     */
    public Itineraire(Intersection depart, Intersection arrive) {
        this.depart = depart;
        this.arrive = arrive;
        duree = -1;
        troncons = new ArrayList<>();
    }
    
    /**
     * Relie entre intersections
     * @param a Intersection
     * @param b Intersection
     * @return boolean
     */
    public boolean relie(Intersection a, Intersection b){
        if(depart == null || arrive == null)
            return false;
        return depart.equals(a) && arrive.equals(b);
    }
    
    /**
     * Ajouter un troncon
     * @param troncon Troncon
     */
    public void ajouterTroncon(Troncon troncon) {
        if(troncons != null)
            troncons.add(troncon);
    }

    /**
     * Rendre la duree 
     * @return duree de l'itineraire
     */
    public double getDuree() {
        if(duree == -1 && troncons != null){
            double sum = 0;
            for(Troncon t : troncons){
                sum += t.calculDuree();
            }
            duree = sum;
        }
        return duree;
    }
    /**
     * Rendre le depart
     * @return intersection de depart
     */
    public Intersection getDepart() {
        return depart;
    }
    /**
     * rendre l'arrive
     * @return intersection d'arrivee
     */
    public Intersection getArrive() {
        return arrive;
    }
    
    /**
     * rendre la liste de troncons
     * @return List 
     */
    public List<Troncon> getTroncons() {
        return troncons;
    }
    
    public void inverserTroncons(){
        List<Troncon> newOrder = new ArrayList<>();
        for(int i = troncons.size()-1 ; i >= 0 ; i--)
            newOrder.add(troncons.get(i));
        troncons = newOrder;
    }
}
