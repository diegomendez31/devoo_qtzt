/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author edelastics
 */
public class Time {
    private int heure;
    private int min;
    private int sec;
    
    /**
     * Constructor 
     * @param dur double
     */
    public Time(double dur){
        int rest = (int)Math.round(dur);
        heure = rest/3600;
        rest -= heure*3600;
        min = rest/60;
        rest -= min;
        sec = rest;
    }
    
    /**
     * Constructor 
     * @param h int
     * @param m int
     * @param s int
     */
    public Time(int h, int m, int s){
        heure = h;
        min = m;
        sec = s;
    }
    
    /**
     * Constructor 
     * @param t Time
     */
    public Time(Time t){
        heure = t.heure;
        min = t.min;
        sec = t.sec;
    }
    
    /**
     * Rendre l'heure
     * @return int
     */
    public int getHours() {
        return heure;
    }
    
    /**
     * Rendre les minutes
     * @return int
     */
    public int getMinutes() {
        return min;
    }
    
    /**
     * Rendre les seconds
     * @return int
     */
    public int getSeconds() {
        return sec;
    }
    
    /**
     * Ajouter temps a l'heure present
     * @param h int
     * @param m int
     * @param s int
     * @return Time
     */
    public Time addTime(int h, int m, int s){
        sec += s;
        while(sec >= 60){
            sec -=60;
            min++;
        }
        min += m;
        while(min >= 60){
            min -= 60;
            heure++;
        }
        heure = (heure+h)%24;
        return this;
    }
    
    /**
     * Ajouter temps a l'heure present
     * @param t Time
     * @return Time
     */
    public Time addTime(Time t){
        return this.addTime(t.heure,t.min,t.sec);
    }
    
    /**
     * Diminuer temps a l'heure present
     * @param h int
     * @param m int
     * @param s int
     * @return Time
     */
    public Time remTime(int h, int m , int s){
        sec -= s;
        while(sec < 0){
            sec +=60;
            min--;
        }
        min -= m;
        while(min < 0){
            min += 60;
            heure--;
        }
        heure = (heure-h)%24;
        return this;
    }
    
    /**
     * Diminuer temps a l'heure present
     * @param t Time
     * @return Time
     */
    public Time remTime(Time t){
        return this.remTime(t.heure, t.min, t.sec);
    }
    
    /**
     * Mettre le temps a un String
     * @return String
     */
    public String timeToString(){
        String x= null;
        x=(heure>9?"":"0") + heure + " : " + (min>9?"":"0") + min + " : " + (sec>9?"":"0") +sec;
        return x;
    }
    
    /**
     * Compare Time
     * @param t the time you want to be compared to
     * @return true if this is before t
     */
    public boolean before(Time t){
        if(t.heure < this.heure)
            return false;
        if(t.heure == this.heure)
            if(t.min < this.min)
                return false;
            else if (t.min == this.min)
                if(t.sec < this.sec)
                    return false;
        
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Time other = (Time) obj;
        if (this.heure != other.heure) {
            return false;
        }
        if (this.min != other.min) {
            return false;
        }
        if (this.sec != other.sec) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Time - " + (heure>9?"":"0") + heure + "\t: " + (min>9?"":"0") + min + "\t: " + (sec>9?"":"0") + sec;
    }
    
    
}
