package model;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import vueP.Observateur;

/**
 * 
 */
public class PlanVille implements Observable { 

    public enum Error {
        ERREUR_1,
        ERREUR_2,
        ERREUR_3;
    }
    private Intersection entrepot;
    private File xml;
    private List<Intersection> intersections;
    private List<Troncon> troncons;
    private List<Livraison> livraisons;
    private List<Observateur> listObserver = new ArrayList<>();
    private Tournee tournee;
    
    public PlanVille(){}
    
//    public File genererFeuilleRoute() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    public Intersection getEntrepot() {
        return entrepot;
    }
    
    /**
     * Constructor
     * @param xml File
     *
     */
    public PlanVille(File xml) {
        entrepot = null;
        this.xml = xml;
        intersections = new ArrayList<>();
        livraisons = new ArrayList<>();
        troncons = new ArrayList<>();
        listObserver =new ArrayList<>();
        tournee = null;
    }
    
    /**
     * Constructor
     * @param inter Liste de Intersections
     *
     */
    public PlanVille(List<Intersection> inter) {
        entrepot = null;
        this.xml = xml;
        intersections = inter;
        livraisons = new ArrayList<>();
        troncons = new ArrayList<>();
    }
   
    
    /**
     * Construction du Plan de Ville a partir d'un fichier
     * @return code d'erreur int
     */
    
    public int ConstructionPlanVilleAPartirFichier()  {
 
        try {
            Document document_xml_plan;
            DocumentBuilder docbuilder_plan;
            
            docbuilder_plan =DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document_xml_plan = docbuilder_plan.parse(xml);
            org.w3c.dom.Element racine = document_xml_plan.getDocumentElement(); 
            NodeList racineNoeuds = racine.getChildNodes();//recupere dans une liste les enfants de la racine 
            String racine_plan= racine.getNodeName();
            if (racine_plan!="Reseau") return 4;
            int nbRacineNoeuds = racineNoeuds.getLength();
            int nbelems = 0;
	    for (int i = 0; i<nbRacineNoeuds; i++) {
	        if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    nbelems ++;
                }
            }
	    for (int i = 0; i<nbRacineNoeuds; i++) {
	        if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
	            org.w3c.dom.Element noeud = (org.w3c.dom.Element) racineNoeuds.item(i);
                    int x,y,id;
		    x = Integer.parseInt(noeud.getAttribute("x"));
                    y = Integer.parseInt(noeud.getAttribute("y"));
                    id = Integer.parseInt(noeud.getAttribute("id"));
                    intersections.add(new Intersection(x,y,id));
                    
                }
            }
            for (int i = 0; i<nbRacineNoeuds; i++) {
	        if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
	            org.w3c.dom.Element noeud = (org.w3c.dom.Element) racineNoeuds.item(i);
                    int id;
                    id = Integer.parseInt(noeud.getAttribute("id"));
 
                    NodeList leTronconSortant = noeud.getElementsByTagName("LeTronconSortant");
                    int nb_de_troncon = leTronconSortant.getLength();
                    Intersection depart = intersections.get(id);
		    for(int j = 0; j<nb_de_troncon; j++) {
		        Element sous_noeud = (Element) leTronconSortant.item(j);
		     
                        String nom;
                        double vitesse, longueur;
                        int idDest;
                        idDest = Integer.parseInt(sous_noeud.getAttribute("idNoeudDestination"));
                        Intersection dest = intersections.get(idDest);
                        String v = sous_noeud.getAttribute("vitesse").replace(',','.');
                        vitesse = Double.parseDouble(v);
                        String l = sous_noeud.getAttribute("longueur").replace(',','.');
                        longueur = Double.parseDouble(l);
                        nom = sous_noeud.getAttribute("nomRue");
                        Troncon t = new Troncon(nom, vitesse, longueur,depart, dest);
                        troncons.add(t);
                        depart.AjouterTroncon(t);
                    }
                }
            }
            notifierObservateurs();
            return 0;
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(PlanVille.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        } catch (SAXException ex) {
            Logger.getLogger(PlanVille.class.getName()).log(Level.SEVERE, null, ex);
            return 2;
        } catch (IOException ex) {
            Logger.getLogger(PlanVille.class.getName()).log(Level.SEVERE, null, ex);
            return 3;
        }
    }
    
    
    /**
     * Calcule le plus court chemin entre les intersection suivants
     * @param start Intersection    
     * @param ends List 
     * @return List
     */
    public List<Itineraire> CalculPlusCourtChemin(Intersection start, List<Intersection> ends) {
        List<Itineraire> ret = new ArrayList<>();
        List<Double> dist = new ArrayList<>();
        List<Integer> pred = new ArrayList<>();
        for(int i = 0 ; i < intersections.size() ; i++){
            dist.add(Double.MAX_VALUE);
            pred.add(-1);
        }
        
        dist.set(start.getId(), 0.0);
        
        List<Intersection> endscpy = new ArrayList<>();
        endscpy.addAll(ends);
        List<Intersection> q = new ArrayList<>();
        q.addAll(intersections);
        
        while(!endscpy.isEmpty() && !q.isEmpty()){
            Intersection s1 = null;
            double min = Double.MAX_VALUE;
            for(Intersection inter : q){
                double val = dist.get(inter.getId());
                if(val < min){
                    s1 = inter;
                    min = val;
                }
            }
            
            for(Intersection s2 : s1.getvoisinsAccessible()){
                int rank1 = s1.getId();
                int rank2 = s2.getId();
                Troncon t = s1.getTronconToVoisin(s2);
                double distance = t.calculDuree();
                if(dist.get(rank2) > dist.get(rank1)+distance){
                    dist.set(rank2, dist.get(rank1)+distance);
                    pred.set(rank2,rank1);
                }
            }
            
            if(endscpy.contains(s1))
                endscpy.remove(s1);
            q.remove(s1);
        }
        
        for(Intersection arr : ends){
            Itineraire it = new Itineraire(start,arr);
            Intersection fin = arr;
            int rankpred = pred.get(arr.getId());
            while(rankpred != -1){
                Intersection deb = intersections.get(rankpred);
                
                Troncon t = null;
                for(Intersection i : deb.getvoisinsAccessible()){
                    if(i.equals(fin)){
                        t = deb.getTronconToVoisin(i);
                        break;
                    }
                }
                it.ajouterTroncon(t);
                //it.inverserOrdre();
                fin = deb;
                rankpred = pred.get(fin.getId());
            }
            it.inverserTroncons();
            it.getDuree();
            ret.add(it);
        }
        
        return ret;
    }

    
    /**
     * Rendre la liste de intersections
     * @return List  
     */
    public List<Intersection> getIntersections() {
        return intersections;
    }
    
   /**
     * Rendre une intersection pour son id
     * @param id int
     * @return Intersection  
     */
    public Intersection getIntersectionByiD(int id) {
        for(Intersection i : intersections){
            if(i.getId() == id)
                return i;
        }
        return null;
    }
    
    /**
     * Lire les livraison qui sont sur un fichier
     * @param livraison File
     * @return Tournee
     */

    
    
    
    public Tournee lireLivraisonsDepuisFichier(File livraison) {
        // TODO implement here
        livraisons.clear();
        tournee = new Tournee(livraison,this);
        notifierObservateurs();
        return tournee;
    }
    
    /**
     * Rendre la liste de troncons
     * @return List  
     */
    public List<Troncon> getTroncons() {
        return troncons;
    }
    
    /**
     * Rendre la tournee
     * @return Tournee  
     */
    public Tournee getTournee(){
        return tournee;
    }
    
    /**
     * Creer une livraison 
     * @param id int
     * @param client int
     * @param adresse int
     * @param uneFenetre FenetreDeTemps
     */
    public void CreerLivraison(int id, int client, int adresse, FenetreDeTemps uneFenetre) {
        // TODO implement here
        Livraison l = new Livraison(id, client, getIntersectionByiD(adresse) , uneFenetre);
        livraisons.add(l);
        uneFenetre.ajouterLivraison(l);
        // a voir comment recuperer les itiniraires ??
    }

    /**
     * Mettre un entrepot sur le plan
     * @param adresse int 
     */
    public void setEntrepot(int adresse){
        entrepot = getIntersectionByiD(adresse);
    }
    
    /**
     * Rendre la liste de livraison sur le plan
     * @return List
     */
    public List<Livraison> getLivraisons() {
        return livraisons;
    }
    /**
     * Rendre le nombre de livrasions
     * @return integer
     */
    public int getNbLivraisons(){
        return livraisons.size();
    }
    
    /**
     * Rendre la livraison pour sa position
     * @param pos int
     * @return Livraison
     */
    public Livraison getLivraison(int pos){
        if(pos > 0 && pos < livraisons.size()){
            return livraisons.get(pos);
        } else {
            return null;
        }
        
    }
    
    /**
     * Rendre la liste de Livraison en iterator
     * @return Iterator  
     */
    public Iterator<Livraison> getIterateurFormes(){
        return livraisons.iterator();
    }
    
    /**
     * Mettre la tournee sur le plan
     * @param tournee Tournee  
     */
    public void setTournee(Tournee tournee) {
        this.tournee = tournee;
    }
    
    
    
    /**
     * Notifier les observateurs
     */
    @Override
    public void notifierObservateurs() {
        for(int i=0;i<listObserver.size();i++)
            {
                Observateur o = listObserver.get(i);
                o.update((model.Observable) this);
            }
        
    }

    /**
     * Suprimmer l'observateur
     * @param obs Observateur
     */
    @Override
    public void supprimerObservateur(Observateur obs) {
        listObserver.remove(obs);
    }

    /**
     * Ajouter l'observateur
     * @param obs Observateur
     */
    @Override
    public void ajouterObservateur(Observateur obs) {
        listObserver.add(obs);
    }

}
