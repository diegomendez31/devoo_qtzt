package model;


public interface VisiteurDeFormes {
    /**
     * Visite de forme Troncon
     * @param t Troncon
     */
    public void visiteForme(Troncon t);
    /**
     * Visite de forme Itineraire
     * @param it Itineraire
     */
    public void visiteForme(Itineraire it);
    /**
     * Visite de forme Intersection 
     * @param iter Intersection
     * @param entrepot boolean
     */
    public void visiteForme(Intersection iter, boolean entrepot);
     /**
     * Visite de forme Tournee 
     * @param t Tournee
     */
    public void visiteForme(Tournee t);
    /**
     * Visite de forme Livraison 
     * @param l Livraison
     */
    public void visiteForme(Livraison l);
}
