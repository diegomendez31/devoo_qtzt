package tests;
import model.PlanVille;
import java.io.File;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class TestChargerLivraison {
	
 
   
   

   @Test 
   public void testPrintMessage() {
       File xml = new File("Bases/plan10x10.xml"); 
       File xmlLiv = new File("Bases/livraison10x10-1.xml"); 
       PlanVille plan = new PlanVille(xml);
       plan.ConstructionPlanVilleAPartirFichier();
       plan.lireLivraisonsDepuisFichier(xmlLiv);
       String x =Integer.toString(plan.getLivraisons().size());
       assertEquals("8",x);

   }
}
