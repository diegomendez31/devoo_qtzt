/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 *
 * @author smankai
 */
public class TestRunner {
    public static void main(String[] args) {
     // Test Charger Plan
        System.out.println("Test 1 : Chargement du plan de la ville");
        Result result1 = JUnitCore.runClasses(TestChargerPlan.class);
      for (Failure failure : result1.getFailures()) {
         System.out.println(failure.toString());
      }
      if (result1.wasSuccessful()){
      System.out.println("Test 1 : OK");
      }
      
      // Test Charger Livraison
      System.out.println("Test 2 : Chargement des Livraisons");
      Result result2 = JUnitCore.runClasses(TestChargerLivraison.class);
      for (Failure failure : result2.getFailures()) {
         System.out.println(failure.toString());
      }
      if (result2.wasSuccessful()){
         System.out.println("Test 2 : OK");
      }
      
      // Test Calcul duree
      System.out.println("Test 3 : Test Calcul Tournée");
      Result result3 = JUnitCore.runClasses(TestCalculTournee.class);
      for (Failure failure : result3.getFailures()) {
         System.out.println(failure.toString());
      }
      if (result3.wasSuccessful()){
         System.out.println("Test 3 : OK");
      }
      
      // Test generation feuille route
      System.out.println("Test 4 : Test génération feuille route");
      Result result4 = JUnitCore.runClasses(TestFeuilleRoute.class);
      for (Failure failure : result4.getFailures()) {
         System.out.println(failure.toString());
      }
      if (result4.wasSuccessful()){
         System.out.println("Test 4 : OK");
      }
      
   }
    
}
