package tests;
import model.PlanVille;
import model.Tournee;
import java.io.File;
import java.sql.Time;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestCalculTournee {
	
  
    

   @Test
   public void testPrintMessage() {
       File xml = new File("Bases/plan10x10.xml"); 
       File xmlLiv = new File("Bases/livraison10x10-1.xml"); 
       PlanVille plan = new PlanVille(xml);
       
       plan.ConstructionPlanVilleAPartirFichier();
       plan.lireLivraisonsDepuisFichier(xmlLiv);
       Tournee t = new Tournee(xmlLiv, plan);
       t.CalculTournee();
       int x = t.getItineraires().size();
       assertEquals(9,x);
      

   }
}