package tests;

import model.PlanVille;
import model.Tournee;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.commons.io.FileUtils;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import vueP.*;
public class TestFeuilleRoute {
    
    File xml = new File("Bases/plan10x10.xml"); 
   PlanVille plan1 = new PlanVille(xml);
    File xmlLiv = new File("Bases/livraison10x10-1.xml");
    
    @Test
   public void testPrintMessage() throws UnsupportedEncodingException, IOException {
        File xml = new File("Bases/plan10x10.xml"); 
        PlanVille plan1 = new PlanVille(xml);
        File xmlLiv = new File("Bases/livraison10x10-2.xml");
        plan1.ConstructionPlanVilleAPartirFichier();
        plan1.lireLivraisonsDepuisFichier(xmlLiv);
        Tournee t = plan1.getTournee();
        t.CalculTournee();
        plan1.setTournee(t);
        FeuilleRoute f = new FeuilleRoute (plan1);
        File x = f.genererFeuilleRoute();
        
        boolean test= FileUtils.contentEquals(x, new File("testfiles/Feuille-Route.txt"));
        
        assertTrue(test);
        
   }
}
