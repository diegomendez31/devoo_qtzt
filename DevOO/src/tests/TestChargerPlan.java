package tests;
import model.PlanVille;
import java.io.File;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class TestChargerPlan {
	
   File xml = new File("Bases/plan10x10.xml"); 
   PlanVille plan = new PlanVille(xml);
    

   @Test
   public void testPrintMessage() {
       plan.ConstructionPlanVilleAPartirFichier();
       String x =Integer.toString(plan.getIntersections().size());
       assertEquals("100",x);

   }
}
